#!/bin/sh

set -e -x -o pipefail

[ -f "/tiddlywiki/tiddlywiki.info" ] || \
    ( echo "Creating new wiki..."; tiddlywiki /tiddlywiki --init)

tiddlywiki /tiddlywiki --listen host=0.0.0.0
