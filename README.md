tiddly-docker
=============

> Let's run [TiddlyWiki](https://tiddlywiki.com/) under [Kubernetes](https://kubernetes.io/)

Building
--------

See `Makefile` for the useful commands.

Data
----

The wiki is started serving `/tiddlywiki` from inside the container.
A persistent directory (mount or volume) should be bound to this path.

Kubernetes
----------

See
[deployment.yaml](https://gitlab.com/scvalex/tiddly-docker/-/blob/master/deployment.yaml)
for an example config.  It binds the pod to a single node and serves
it on port 30000.
