.PHONY: docker push

docker:
	docker build -t "registry.gitlab.com/scvalex/tiddly-docker:$(shell git branch --show-current)" .

push: docker
	docker push "registry.gitlab.com/scvalex/tiddly-docker:$(shell git branch --show-current)"

run: docker
	docker run --interactive --tty --rm --publish 8080:8080 registry.gitlab.com/scvalex/tiddly-docker:master
