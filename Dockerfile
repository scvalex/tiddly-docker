FROM node:15.6.0-alpine3.11

EXPOSE 8080

RUN npm install -g tiddlywiki@5.1.23

# Where to store tiddlywiki data
RUN mkdir /tiddlywiki
# RUN chown nobody:nobody /tiddlywiki

COPY start_tiddlywiki.sh /
# RUN chown nobody:nobody /start_tiddlywiki.sh

# USER nobody:nobody

CMD ["/start_tiddlywiki.sh"]
